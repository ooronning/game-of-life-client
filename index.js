const urlRoot = 'https://localhost:44387';

// ----
// Utils

const intRange = (startAt, endAt) => {
  return Array.from(Array(endAt - startAt + 1).keys()).map(x => x + startAt);
};

// -----
// Enums

const patterns = {
  Glider: 'Glider',
  Blinker: 'Blinker',
  QueenBee: 'QueenBee',
  GliderIntoBlock: 'GliderIntoBlock'
};

const patternFiles = [
  {
    name: 'Gosper Glider Gun',
    path: 'patterns/gosperglidergun.lif'
  }
]

// -----
// Set up field visualization

const liveIndices = [];

// constants
const cellWidth = 15;
const cellHeight = 15;

const horizontalCells = 50;
const vertCells = 50;

const gridWidth = horizontalCells * cellWidth;
const gridHeight = vertCells * cellHeight;

console.log(gridWidth + ' px');
console.log(gridHeight + ' px');

const canvas = document.createElement('canvas');
canvas.width = gridWidth;
canvas.height = gridHeight;
canvas.style.border = '1px solid black';
const ctx = canvas.getContext('2d');

const dead = 'white';
const alive = 'black';

function generateDeadField() {
  for (let x = 0; x < horizontalCells; x++) {
    for (let y = 0; y < vertCells; y++) {
      ctx.fillStyle = dead;
      ctx.fillRect((x * cellWidth), (y * cellHeight), cellWidth, cellHeight);
    }
  }
}

function updateField(cells) {
  liveIndices.length = 0;
  for (const [i, row] of cells.entries()) {
    for (const [j, cell] of row.entries()) {
      if (cell.Alive) {
        ctx.fillStyle = alive;
        liveIndices.push(cell);
      } else {
        ctx.fillStyle = dead;
      }
      ctx.fillRect((i * cellWidth), (j * cellHeight), cellWidth, cellHeight);
    }
  }

  // console.log('liveIndices', liveIndices);
}

// -----
// Misc UI setup

// patterns dropdown
const dropdown = document.querySelector('.user-input select');
for (const p in patterns) {
  const option = document.createElement('option');
  option.value = p;
  option.innerText = p;
  dropdown.appendChild(option);
}


const filesUl = document.querySelector('.pattern-files ul');
for (const fileObj of patternFiles) {
  console.log(fileObj);
  const li = document.createElement('li');
  const a = document.createElement('a');
  a.innerText = fileObj.name;
  a.href = fileObj.path;
  // a.download = file.name.split(' ').join('') + '.lif';
  a.addEventListener('click', async e => {
    e.preventDefault();

    try {
      const file = await fetchFile(fileObj.path, {
        mode: "cors"
      });
      const formData = new FormData();
      formData.append('file', file);

      const data = await fetch(`${urlRoot}/api/field/file_upload`, {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        body: formData
      });

      console.log(data.json());
    } catch (e) {
      console.error(e.toString());
    }
  })

  filesUl.appendChild(li);
  li.appendChild(a);
}

async function fetchFile(path) {
  try {
    const file = await fetch(path);
    return file;
  } catch (e) {
    console.error(e.toString());
  }
}

async function uploadFile(file ) {
  try {
    const data = await fetch(`${urlRoot}/api/field/file_upload`, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    });

    console.log(data.json());
  } catch (e) {
    console.error(e.toString());
  }
}

// -----
// Set up websocket connection and related methods

// configure a connection with our server hub. This allows us to open a websocket
const connection = new signalR.HubConnectionBuilder().withUrl(`${urlRoot}/game-of-life/field`).build();

// handle response from socket
let running = false;
let runningInterval;

connection.on('ReceiveField', response => {
  const field = JSON.parse(response);
  updateField(field.Cells);
});

document.querySelector('.user-input__start').addEventListener('click', async e => {
  e.preventDefault();
  liveIndices.length = 0;

  const pattern = dropdown.options[dropdown.selectedIndex].value;
  try {
    await connection.invoke('InitializeField', pattern);
  } catch (e) {
    console.error(e.toString());
  }
});

document.querySelector('.user-input__next').addEventListener('click', async e => {
  e.preventDefault();
  console.log('here');
  try {
    await connection.invoke('UpdateField');
  } catch (e) {
    console.error(e.toString());
  }
});

document.querySelector('.user-input__run').addEventListener('click', async e => {
  e.preventDefault();
  runningInterval = window.setInterval(async () => {
    try {
      await connection.invoke('UpdateField');
    } catch (e) {
      console.error(e.toString());
    }
  }, 100);
});

document.querySelector('.user-input__stop').addEventListener('click', () => {
  clearInterval(runningInterval);
});

// -----
// Initialize app

generateDeadField();
document.querySelector('.content').appendChild(canvas);
connection.start().catch(e => console.error(e.toString()));
