const urlRoot = 'https://localhost:44387';

const connection = new signalR.HubConnectionBuilder().withUrl(`${urlRoot}/test-hub`).build();
const list = document.querySelector('#counter-list');

connection.on('ReceiveCount', (counter) => {
  const message = `Current count is: ${counter}`;
  const li = document.createElement('li');
  li.textContent = message;
  list.appendChild(li);
});

document.querySelector('#count-button').addEventListener('click', async e => {
  e.preventDefault();
  try {
    await connection.invoke('SendCount');
  } catch(e) {
    console.error(e.toString());
  }
});

connection.start().catch(e => {
  console.error(e.toString());
});
